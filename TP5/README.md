# TP5 : Installation et configuration de Seafile

# Sommaire

- [Sommaire](#sommaire)
- [0. Prérequis](#0-prérequis)
- [I. Un premier serveur web](#i-un-premier-serveur-web)
  - [1. Installation](#1-installation)
- [II. Une stack web plus avancée](#ii-une-stack-web-plus-avancée)
  - [1. Intro blabla](#1-intro-blabla)
  - [2. Setup](#2-setup)
    - [A. Base de données](#a-base-de-données)
    - [B. Serveur Web et NextCloud](#b-serveur-web-et-nextcloud)
    - [C. Finaliser l'installation de NextCloud](#c-finaliser-linstallation-de-nextcloud)

# 0. Prérequis

➜ Machines Rocky Linux

➜ Un unique host-only côté VBox, ça suffira. **L'adresse du réseau host-only sera `10.105.1.0/24`.**

➜ Chaque **création de machines** sera indiquée par **l'emoji 🖥️ suivi du nom de la machine**

## I. Installation et configuration de MariaDB

🖥️ **VM db.tp2.linux**


| Machines        | IP            | Service                 |
|-----------------|---------------|-------------------------|
| `web.tp5.linux` | `10.105.1.11` | Serveur Web             |
| `db.tp5.linux`  | `10.105.1.12` | Serveur Base de Données |


**Install de MariaDB sur `db.tp5.linux`**

```
sudo dnf install mariadb-server -y
sudo systemctl start mariadb
sudo systemctl enable mariadb   
sudo systemctl status mariadb
```

```
sudo mysql_secure_installation

NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user.  If you've just installed MariaDB, and
you haven't set the root password yet, the password will be blank,
so you should just press enter here.

Enter current password for root (enter for none): 
OK, successfully used password, moving on...

Setting the root password ensures that nobody can log into the MariaDB
root user without the proper authorisation.

Set root password? [Y/n] n
 ... skipping.

By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n] n
 ... skipping.

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n] n
 ... skipping.

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n] n
 ... skipping.

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n] n
 ... skipping.

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
```

**Préparation de la base pour Seafile**

```
mysql -u root -p
```

**Création des databases**

```
CREATE DATABASE ccnet_server;
CREATE DATABASE seahub_server;
CREATE DATABASE seafile_server;
```

**Création du user seafile**

```
CREATE USER 'seafile'@'web' IDENTIFIED BY 'toto';
GRANT ALL PRIVILEGES ON ccnet_server.* TO 'seafile'@'web';
GRANT ALL PRIVILEGES ON seahub_server.* TO 'seafile'@'web';
GRANT ALL PRIVILEGES ON seafile_server.* TO 'seafile'@'web';
FLUSH PRIVILEGES
```

**Configuration du firewall**

```
sudo firewall-cmd --add-port 3306/tcp
firewall-cmd --reload
```


# II. Le serveur web

## 1. Installation de Seafile Server

🖥️ **VM web.tp2.linux**

| Machine         | IP            | Service     |
|-----------------|---------------|-------------|
| `web.tp5.linux` | `10.105.1.11` | Serveur Web |


**Installation des prerequis**

```
sudo yum install python3 python3-setuptools python3-pip python3-devel mysql-devel gcc -y
sudo pip3 install --timeout=3600 Pillow pylibmc captcha jinja2 sqlalchemy==1.4.3 \
    django-pylibmc django-simple-captcha python3-ldap mysqlclient
```

**Création du dossier du programme**

```
mkdir /opt/seafile
cd /opt/seafile
```

**Création du user seafile**

```
adduser seafile
chown -R seafile: /opt/seafile
su seafile
```

**Récupération du paquet d'installation**

```
sudo dnf install wget
wget https://s3.eu-central-1.amazonaws.com/download.seadrive.org/seafile-server_8.0.7_x86-64.tar.gz
```

```
tar xf seafile-server_8.0.4_x86-64.tar.gz
```

**Setup de Seafile**

```
cd seafile-server-8.0.4
./setup-seafile-mysql.sh

Checking python on this machine ...
Checking python module: setuptools ... Done.
Checking python module: python-imaging ... Done.
Checking python module: python-mysqldb ... Done.

-----------------------------------------------------------------
This script will guide you to setup your seafile server using MySQL.
Make sure you have read seafile server manual at

        https://download.seafile.com/published/seafile-manual/home.md

Press ENTER to continue
-----------------------------------------------------------------


What is the name of the server? It will be displayed on the client.
3 - 15 letters or digits
[ server name ] server.seafile

What is the ip or domain of the server?
For example: www.mycompany.com, 192.168.1.101
[ This server's ip or domain ] server.seafile.com

Which port do you want to use for the seafile fileserver?
[ default "8082" ] <ENTER>

-------------------------------------------------------
Please choose a way to initialize seafile databases:
-------------------------------------------------------

[1] Create new ccnet/seafile/seahub databases
[2] Use existing ccnet/seafile/seahub databases

[ 1 or 2 ] 2

What is the host of mysql server?
[ default "localhost" ] 10.105.1.12

What is the port of mysql server?
[ default "3306" ] <ENTER>

Which mysql user to use for seafile?
[ mysql user for seafile ] seafile

What is the password for mysql user "seafile"?
[ password for seafile ] toto

verifying password of user seafile ...  done

Enter the existing database name for ccnet:
[ ccnet database ] ccnet_server

verifying user "seafile" access to database ccnet_server ...  done

Enter the existing database name for seafile:
[ seafile database ] seafile_server

verifying user "seafile" access to database seafile_server ...  done

Enter the existing database name for seahub:
[ seahub database ] seahub_server

verifying user "seafile" access to database seahub_server ...  done

---------------------------------
This is your configuration
---------------------------------

    server name:            server.seafile
    server ip/domain:       server.seafile.com

    seafile data dir:       /opt/seafile/seafile-data
    fileserver port:        8082

    database:               use existing
    ccnet database:         ccnet_server
    seafile database:       seafile_server
    seahub database:        seahub_server
    database user:          seafile

---------------------------------
Press ENTER to continue, or Ctrl-C to abort
---------------------------------

Generating ccnet configuration ...

Generating seafile configuration ...

done
Generating seahub configuration ...

----------------------------------------
Now creating ccnet database tables ...

----------------------------------------
----------------------------------------
Now creating seafile database tables ...

----------------------------------------
----------------------------------------
Now creating seahub database tables ...

----------------------------------------

creating seafile-server-latest symbolic link ...  done

-----------------------------------------------------------------
Your seafile server configuration has been finished successfully.
-----------------------------------------------------------------

run seafile server:     ./seafile.sh { start | stop | restart }
run seahub  server:     ./seahub.sh  { start <port> | stop | restart <port> }

-----------------------------------------------------------------
If you are behind a firewall, remember to allow input/output of these tcp ports:
-----------------------------------------------------------------

port of seafile fileserver:   8082
port of seahub:               8000

When problems occur, Refer to

        https://download.seafile.com/published/seafile-manual/home.md

for information.
```

**Démarrage du server Seafile**

```
./seafile.sh start 
./seahub.sh start 
```

**Exécuter Seahub sur un autre port**

- modifier ```conf/gunicorn.conf````

```
# default localhost:8000
bind = "0.0.0.0:8001"
```

- redémarrer les services

```
./seafile.sh restart    
./seahub.sh restart  

```

**Installer le serveur Nginx**

```
sudo dnf install nginx -y
sudo systemctl start nginx
sudo systemctl enable nginx
sudo systemctl status nginx
```

```
touch /etc/nginx/conf.d/seafile.conf
server {
    listen 80;
    server_name server.seafile.com;

    proxy_set_header X-Forwarded-For $remote_addr;

    location / {
         proxy_pass         http://127.0.0.1:8000;
         proxy_set_header   Host $host;
         proxy_set_header   X-Real-IP $remote_addr;
         proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
         proxy_set_header   X-Forwarded-Host $server_name;
         proxy_read_timeout  1200s;

         # used for view/edit office file via Office Online Server
         client_max_body_size 0;

         access_log      /var/log/nginx/seahub.access.log seafileformat;
         error_log       /var/log/nginx/seahub.error.log;
    }
    location /seafhttp {
        rewrite ^/seafhttp(.*)$ $1 break;
        proxy_pass http://127.0.0.1:8082;
        client_max_body_size 0;
        proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;

        proxy_connect_timeout  36000s;
        proxy_read_timeout  36000s;
        proxy_send_timeout  36000s;

        send_timeout  36000s;

        access_log      /var/log/nginx/seafhttp.access.log seafileformat;
        error_log       /var/log/nginx/seafhttp.error.log;
    }
    location /media {
        root /opt/seafile/seafile-server-latest/seahub;
    }
```

```
nginx -t
nginx -s reload
```


