# TP2 : Gestion de service

# I. Un premier serveur web

## 1. Installation

🌞 **Installer le serveur Apache**

```
[ml@web ~]$ sudo dnf -y install httpd
Installed:
  apr-1.7.0-11.el9.x86_64                  apr-util-1.6.1-20.el9.x86_64        apr-util-bdb-1.6.1-20.el9.x86_64   apr-util-openssl-1.6.1-20.el9.x86_64   httpd-2.4.51-7.el9_0.x86_64    
  httpd-filesystem-2.4.51-7.el9_0.noarch   httpd-tools-2.4.51-7.el9_0.x86_64   mailcap-2.1.49-5.el9.noarch        mod_http2-1.15.19-2.el9.x86_64         mod_lua-2.4.51-7.el9_0.x86_64  
  rocky-logos-httpd-90.11-1.el9.noarch    

Complete!

```

🌞 **Démarrer le service Apache**

```
[ml@web ~]$ sudo systemctl start httpd
[ml@web ~]$ sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
[ml@web ~]$ sudo ss -ltpn | grep -w 'httpd'
LISTEN 0      511                *:80              *:*    users:(("httpd",pid=1199,fd=4),("httpd",pid=1198,fd=4),("httpd",pid=1197,fd=4),("httpd",pid=1195,fd=4))
[ml@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
```

🌞 **TEST**

```
[ml@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; vendor preset: disabled)
     Active: active (running) since Tue 2022-11-15 14:16:35 CET; 4s ago
       Docs: man:httpd.service(8)
   Main PID: 1280 (httpd)
     Status: "Started, listening on: port 80"
      Tasks: 213 (limit: 5907)
     Memory: 23.1M
        CPU: 71ms
     CGroup: /system.slice/httpd.service
             ├─1280 /usr/sbin/httpd -DFOREGROUND
             ├─1281 /usr/sbin/httpd -DFOREGROUND
             ├─1282 /usr/sbin/httpd -DFOREGROUND
             ├─1283 /usr/sbin/httpd -DFOREGROUND
             └─1284 /usr/sbin/httpd -DFOREGROUND

Nov 15 14:16:35 web.tp2.linux systemd[1]: Starting The Apache HTTP Server...
Nov 15 14:16:35 web.tp2.linux systemd[1]: Started The Apache HTTP Server.
Nov 15 14:16:35 web.tp2.linux httpd[1280]: Server configured, listening on: port 80
[ml@web ~]$ sudo systemctl is-enabled httpd
enabled
[ml@web ~]$ curl localhost
<!doctype html>
<html>
...
</html>
```

## 2. Avancer vers la maîtrise du service

🌞 **Le service Apache...**

```
[ml@web ~]$ sudo systemctl cat httpd
[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
# Send SIGWINCH for graceful stop
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true
OOMPolicy=continue

[Install]
WantedBy=multi-user.target
```

🌞 **Déterminer sous quel utilisateur tourne le processus Apache**

```
[ml@web ~]$ cat /etc/httpd/conf/httpd.conf
User apache
[ml@web ~]$ ps -ef
UID          PID    PPID  C STIME TTY          TIME CMD
apache      1196    1195  0 14:31 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1197    1195  0 14:31 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1198    1195  0 14:31 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1199    1195  0 14:31 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
[ml@web ~]$ ls -al /usr/share/testpage/
-rw-r--r--.  1 root root 7620 Jul  6 04:37 index.html
```

🌞 **Changer l'utilisateur utilisé par Apache**

```
[ml@web ~]$ sudo cat /etc/passwd
apache:x:48:48:Apache:/usr/share/httpd:/sbin/nologin
[ml@web ~]$ sudo useradd toto -m -d /usr/share/httpd -s /sbin/nologin
[ml@web ~]$ sudo cat /etc/passwd
apache:x:48:48:Apache:/usr/share/httpd:/sbin/nologin
toto:x:1001:1001::/usr/share/httpd:/sbin/nologin
[ml@web ~]$ sudo usermod -aG apache toto
[ml@web ~]$ sudo cat /etc/httpd/conf/httpd.conf
User toto
Group apache
[ml@web ~]$ sudo systemctl restart httpd
[ml@web ~]$ ps aux | grep -w 'httpd'
root        1598  0.0  1.2  20304 11816 ?        Ss   15:30   0:00 /usr/sbin/httpd -DFOREGROUND
toto        1600  0.0  0.7  21576  7400 ?        S    15:30   0:00 /usr/sbin/httpd -DFOREGROUND
toto        1601  0.0  1.3 1210512 13200 ?       Sl   15:30   0:00 /usr/sbin/httpd -DFOREGROUND
toto        1602  0.0  1.1 1079376 11152 ?       Sl   15:30   0:00 /usr/sbin/httpd -DFOREGROUND
toto        1603  0.0  1.1 1079376 11152 ?       Sl   15:30   0:00 /usr/sbin/httpd -DFOREGROUND
```

🌞 **Faites en sorte que Apache tourne sur un autre port**

```
[ml@web ~]$ sudo cat /etc/httpd/conf/httpd.conf
Listen 81
[ml@web ~]$ sudo firewall-cmd --add-port=81/tcp --permanent
success
[ml@web ~]$ sudo firewall-cmd --remove-port=80/tcp --permanent
success
[ml@web ~]$ sudo systemctl restart httpd
[ml@web ~]$ sudo ss -ltpn | grep -w 'httpd'
LISTEN 0      511                *:81              *:*    users:(("httpd",pid=1846,fd=4),("httpd",pid=1845,fd=4),("httpd",pid=1844,fd=4),("httpd",pid=1841,fd=4))
[ml@web ~]$ curl localhost:81
<!doctype html>
<html>
...  
</html>
```

📁 **Fichier `/etc/httpd/conf/httpd.conf`**

```
MacBook-Pro-de-saperlipopette:~ marieliserenzema$ scp ml@10.102.1.11:/etc/httpd/conf/httpd.conf ./Documents/
httpd.conf                                    100% 1509     2.5MB/s   00:00
```

![Fichier Conf](./docs/httpd.conf)

# II. Une stack web plus avancée

## 2. Setup

### A. Base de données

🌞 **Install de MariaDB sur `db.tp2.linux`**

```
[ml@db ~]$ sudo dnf install mariadb-server
Complete!
[ml@db ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.
[ml@db ~]$ sudo systemctl start mariadb
[ml@db ~]$ sudo mysql_secure_installation
[ml@db ~]$ sudo ss -ltpn | grep -w 'mariadbd'
LISTEN 0      80                 *:3306            *:*    users:(("mariadbd",pid=986,fd=18))
[ml@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
success
```

🌞 **Préparation de la base pour NextCloud**

```
[ml@db ~]$ sudo mysql -u root -p
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 17
Server version: 10.5.16-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'pewpewpew';
Query OK, 0 rows affected (0.003 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.001 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';
Query OK, 0 rows affected (0.002 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> exit
Bye
```

🌞 **Exploration de la base de données**

```
[ml@web ~]$ sudo dnf provides mysql
[ml@web ~]$ sudo dnf install mysql-8.0.30-3.el9_0.x86_64
[ml@web ~]$ mysql -u nextcloud -h 10.102.1.12 -p
```
```sql
mysql> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.00 sec)

mysql> USE nextcloud;
Database changed
mysql> SHOW TABLES;
Empty set (0.00 sec)
```

🌞 **Trouver une commande SQL qui permet de lister tous les utilisateurs de la base de données**

```
[ml@db ~]$ sudo mysql -u root -p

MariaDB [nextcloud]> use mysql;
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
MariaDB [mysql]> show tables;
+---------------------------+
| Tables_in_mysql           |
+---------------------------+
| time_zone_transition_type |
| transaction_registry      |
| user                      |
+---------------------------+
31 rows in set (0.000 sec)

MariaDB [mysql]> select user from user;
+-------------+
| User        |
+-------------+
| nextcloud   |
| mariadb.sys |
| mysql       |
| root        |
+-------------+
4 rows in set (0.001 sec)
```

### B. Serveur Web et NextCloud



🌞 **Install de PHP**

```
[ml@web ~]$ sudo dnf config-manager --set-enabled crb
[ml@web ~]$ sudo dnf install dnf-utils http://rpms.remirepo.net/enterprise/remi-release-9.rpm -y
[ml@web ~]$ dnf module list php
[ml@web ~]$ sudo dnf module enable php:remi-8.1 -y
[ml@web ~]$ sudo dnf install -y php81-php
```

🌞 **Install de tous les modules PHP nécessaires pour NextCloud**

```
[ml@web ~]$ sudo dnf install -y libxml2 openssl php81-php php81-php-ctype php81-php-curl php81-php-gd php81-php-iconv php81-php-json php81-php-libxml php81-php-mbstring php81-php-openssl php81-php-posix php81-php-session php81-php-xml php81-php-zip php81-php-zlib php81-php-pdo php81-php-mysqlnd php81-php-intl php81-php-bcmath php81-php-gmp
```

🌞 **Récupérer NextCloud**

```
[ml@web www]$ sudo mkdir tp2_nextcloud
[ml@web tp2_nextcloud]$ sudo curl -SLO https://download.nextcloud.com/server/prereleases/nextcloud-25.0.0rc3.zip
[ml@web tp2_nextcloud]$ sudo dnf install unzip
[ml@web tp2_nextcloud]$ sudo unzip nextcloud-25.0.0rc3.zip
[ml@web www]$ sudo mv tp2_nextcloud/nextcloud/* tp2_nextcloud/
[ml@web tp2_nextcloud]$ cat index.html
<!DOCTYPE html>
<html>
<head>
  <script> window.location.href="index.php"; </script>
  <meta http-equiv="refresh" content="0; URL=index.php">
</head>
</html>
[ml@web ~]$ sudo chown apache /var/www/tp2_nextcloud/*
[ml@web ~]$ ls -al /var/www/tp2_nextcloud/
total 132
drwxr-xr-x. 14 apache root  4096 Nov 15 17:30 .
drwxr-xr-x.  5 root   root    54 Nov 15 17:17 ..
drwxr-xr-x. 47 apache root  4096 Oct  6 14:47 3rdparty
drwxr-xr-x. 50 apache root  4096 Oct  6 14:44 apps
-rw-r--r--.  1 apache root 19327 Oct  6 14:42 AUTHORS
drwxr-xr-x.  2 apache root    67 Oct  6 14:47 config
-rw-r--r--.  1 apache root  4095 Oct  6 14:42 console.php
-rw-r--r--.  1 apache root 34520 Oct  6 14:42 COPYING
drwxr-xr-x. 23 apache root  4096 Oct  6 14:47 core
-rw-r--r--.  1 apache root  6317 Oct  6 14:42 cron.php
drwxr-xr-x.  2 apache root  8192 Oct  6 14:42 dist
-rw-r--r--.  1 apache root   156 Oct  6 14:42 index.html
-rw-r--r--.  1 apache root  3456 Oct  6 14:42 index.php
drwxr-xr-x.  6 apache root   125 Oct  6 14:42 lib
-rw-r--r--.  1 apache root   283 Oct  6 14:42 occ
drwxr-xr-x.  2 apache root    23 Oct  6 14:42 ocm-provider
drwxr-xr-x.  2 apache root    55 Oct  6 14:42 ocs
drwxr-xr-x.  2 apache root    23 Oct  6 14:42 ocs-provider
-rw-r--r--.  1 apache root  3139 Oct  6 14:42 public.php
-rw-r--r--.  1 apache root  5426 Oct  6 14:42 remote.php
drwxr-xr-x.  4 apache root   133 Oct  6 14:42 resources
-rw-r--r--.  1 apache root    26 Oct  6 14:42 robots.txt
-rw-r--r--.  1 apache root  2452 Oct  6 14:42 status.php
drwxr-xr-x.  3 apache root    35 Oct  6 14:42 themes
drwxr-xr-x.  2 apache root    43 Oct  6 14:44 updater
-rw-r--r--.  1 apache root   387 Oct  6 14:47 version.php
```

🌞 **Adapter la configuration d'Apache**

```
[ml@web ~]$ sudo cat /etc/httpd/conf/httpd.conf
IncludeOptional conf.d/*.conf
[ml@web ~]$ cd /etc/httpd/conf.d/
[ml@web conf.d]$ cat tp2.conf
<VirtualHost *:80>
  # on indique le chemin de notre webroot
  DocumentRoot /var/www/tp2_nextcloud/
  # on précise le nom que saisissent les clients pour accéder au service
  ServerName  web.tp2.linux

  # on définit des règles d'accès sur notre webroot
  <Directory /var/www/tp2_nextcloud/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews
    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
```

🌞 **Redémarrer le service Apache** pour qu'il prenne en compte le nouveau fichier de conf

```
[ml@web ~]$ sudo systemctl restart httpd
```

### C. Finaliser l'installation de NextCloud

🌞 **Exploration de la base de données**

```
[ml@web ~]$ mysql -u nextcloud -h 10.102.1.12 -p
```

```sql
mysql> SELECT count(*) AS TOTALNUMBEROFTABLES
    -> FROM INFORMATION_SCHEMA.TABLES
    -> WHERE TABLE_SCHEMA = 'nextcloud';
+---------------------+
| TOTALNUMBEROFTABLES |
+---------------------+
|                  95 |
+---------------------+
1 row in set (0.00 sec)
```
