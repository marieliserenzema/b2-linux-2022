# TP4 : Conteneurs

# I. Docker

🖥️ Machine **docker1.tp4.linux**

## 1. Install

🌞 **Installer Docker sur la machine**

- Installation 

```
[ml@docker1 ~]$ sudo dnf config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo
[ml@docker1 ~]$ sudo dnf update
[ml@docker1 ~]$ sudo dnf install docker-ce docker-ce-cli containerd.io
Complete!
```

- Démarrage du service 

```
[ml@docker1 ~]$  sudo systemctl start docker
[ml@docker1 ~]$  sudo systemctl status docker
● docker.service - Docker Application Container Engine
     Loaded: loaded (/usr/lib/systemd/system/docker.service; disabled; vendor p>
     Active: active (running) since Thu 2022-11-24 15:57:13 CET; 2s ago
TriggeredBy: ● docker.socket
       Docs: https://docs.docker.com
   Main PID: 46673 (dockerd)
      Tasks: 7
     Memory: 22.8M
        CPU: 78ms
     CGroup: /system.slice/docker.service
             └─46673 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/con>
[ml@docker1 ~]$  sudo systemctl enable docker
Created symlink /etc/systemd/system/multi-user.target.wants/docker.service → /usr/lib/systemd/system/docker.service.
[ml@docker1 ~]$  sudo systemctl is-enabled docker
enabled
```

- Ajout de mon utilisateur au groupe `docker`

```
[ml@docker1 ~]$ sudo usermod -aG docker $(whoami)
[ml@docker1 ~]$ exit
logout
Connection to 10.104.1.11 closed.
MacBook-Pro-de-saperlipopette:~ marieliserenzema$ ssh ml@10.104.1.11
Last login: Thu Nov 24 14:12:30 2022 from 10.104.1.1
[ml@docker1 ~]$
```


## 2. Vérifier l'install

➜ **Vérifiez que Docker est actif est disponible en essayant quelques commandes usuelles :**

```bash
[ml@docker1 ~]$ docker info
Client:
 Context:    default
 Debug Mode: false
 Plugins:
  app: Docker App (Docker Inc., v0.9.1-beta3)
  buildx: Docker Buildx (Docker Inc., v0.9.1-docker)
  scan: Docker Scan (Docker Inc., v0.21.0)

Server:
 Containers: 0
  Running: 0
  Paused: 0
  Stopped: 0
 Images: 0
 Server Version: 20.10.21
 Storage Driver: overlay2
  Backing Filesystem: xfs
  Supports d_type: true
  Native Overlay Diff: true
  userxattr: false
 Logging Driver: json-file
 Cgroup Driver: systemd
 Cgroup Version: 2
 Plugins:
  Volume: local
  Network: bridge host ipvlan macvlan null overlay
  Log: awslogs fluentd gcplogs gelf journald json-file local logentries splunk syslog
 Swarm: inactive
 Runtimes: io.containerd.runc.v2 io.containerd.runtime.v1.linux runc
 Default Runtime: runc
 Init Binary: docker-init
 containerd version: 770bd0108c32f3fb5c73ae1264f7e503fe7b2661
 runc version: v1.1.4-0-g5fd4c4d
 init version: de40ad0
 Security Options:
  seccomp
   Profile: default
  cgroupns
 Kernel Version: 5.14.0-70.26.1.el9_0.x86_64
 Operating System: Rocky Linux 9.0 (Blue Onyx)
 OSType: linux
 Architecture: x86_64
 CPUs: 1
 Total Memory: 960.6MiB
 Name: docker1.tp4.linux
 ID: VKTW:P3KZ:CGEZ:AGRF:OGAC:36ST:XRLJ:LFAE:ZNVP:6UGK:XZ5V:BZIS
 Docker Root Dir: /var/lib/docker
 Debug Mode: false
 Registry: https://index.docker.io/v1/
 Labels:
 Experimental: false
 Insecure Registries:
  127.0.0.0/8
 Live Restore Enabled: false
[ml@docker1 ~]$ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
[ml@docker1 ~]$ docker ps -a
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
[ml@docker1 ~]$ docker images
REPOSITORY   TAG       IMAGE ID   CREATED   SIZE
[ml@docker1 ~]$ docker run debian
Unable to find image 'debian:latest' locally
latest: Pulling from library/debian
a8ca11554fce: Pull complete
Digest: sha256:3066ef83131c678999ce82e8473e8d017345a30f5573ad3e44f62e5c9c46442b
Status: Downloaded newer image for debian:latest
[ml@docker1 ~]$ docker images
REPOSITORY   TAG       IMAGE ID       CREATED      SIZE
debian       latest    c31f65dd4cc9   9 days ago   124MB
[ml@docker1 ~]$ docker ps -a
CONTAINER ID   IMAGE     COMMAND   CREATED          STATUS                      PORTS     NAMES
3106ef9d2c08   debian    "bash"    41 seconds ago   Exited (0) 41 seconds ago             admiring_benz
```

➜ **Explorer un peu le help**, si c'est pas le man :

```bash
[ml@docker1 ~]$ docker --help
```

## 3. Lancement de conteneurs

🌞 **Utiliser la commande `docker run`**

- lancer un conteneur `nginx`
  - l'app NGINX doit avoir un fichier de conf personnalisé
  - l'app NGINX doit servir un fichier `index.html` personnalisé
  - l'application doit être joignable grâce à un partage de ports
  - vous limiterez l'utilisation de la RAM et du CPU de ce conteneur
  - le conteneur devra avoir un nom

```
[ml@docker1 ~]$ docker run --name web.tp4 --cpus="1.0" --memory="1g"  -p 8888:80 -v index.html:/var/www/tp4/index.html -v /home/ml/tp4.conf:/etc/nginx/conf.d/tp4.conf --rm nginx
```

```
[ml@docker1 ~]$ sudo cat tp4.conf
server {
  # on définit le port où NGINX écoute dans le conteneur
  listen 9999;

  # on définit le chemin vers la racine web
  # dans ce dossier doit se trouver un fichier index.html
  root /var/www/tp4;
}

[ml@docker1 ~]$ sudo cat index.html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>TP4</title>
  </head>
  <body>
    HELLO WORLD !
  </body>
</html>
```

# II. Images

## 1. Construisez votre propre Dockerfile

🌞 **Construire votre propre image**

- image de base (celle que vous voulez : debian, alpine, ubuntu, etc.)
  - une image du Docker Hub
  - qui ne porte aucune application par défaut
- vous ajouterez
  - mise à jour du système
  - installation de Apache 
  - page d'accueil Apache HTML personnalisée


```
[ml@docker1 work]$ sudo cat index.html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>TP4</title>
  </head>
  <body>
    HELLO WORLD !
  </body>
</html>
[ml@docker1 work]$ sudo cat apache_custom.conf
# on définit un port sur lequel écouter
Listen 80

# on charge certains modules Apache strictement nécessaires à son bon fonctionnement
LoadModule mpm_event_module "/usr/lib/apache2/modules/mod_mpm_event.so"
LoadModule dir_module "/usr/lib/apache2/modules/mod_dir.so"
LoadModule authz_core_module "/usr/lib/apache2/modules/mod_authz_core.so"

# on indique le nom du fichier HTML à charger par défaut
DirectoryIndex index.html
# on indique le chemin où se trouve notre site
DocumentRoot "/var/www/html/"

# quelques paramètres pour les logs
ErrorLog "logs/error.log"
LogLevel warn
```

📁 **`Dockerfile`**

![Dockerfile](./Dockerfile)

```
[ml@docker1 work]$ docker build . -t my_own_apache
[ml@docker1 work]$ docker images
REPOSITORY      TAG       IMAGE ID       CREATED             SIZE
my_own_apache   latest    d9bf28d1cbf3   21 minutes ago      225MB
[ml@docker1 work]$ docker run -p 8888:80 my_own_apache
```

```
[ml@docker1 work]$ curl 10.104.1.11:8888
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>TP4</title>
  </head>
  <body>
    HELLO WORLD !
  </body>
</html>
```

# III. `docker-compose`

## 1. Intro

➜ **Installer `docker-compose` sur la machine**

```
[ml@docker1 ~]$ sudo yum install docker-compose-plugin
Complete!
```

## 2. Make your own meow

🌞 **Conteneurisez votre application**

  - indiquer la commande `git clone`
  - le `cd` dans le bon dossier
  - la commande `docker build` pour build l'image
  - la commande `docker-compose` pour lancer le(s) conteneur(s)

📁 📁 `app/Dockerfile` et `app/docker-compose.yml`. Je veux un sous-dossier `app/` sur votre dépôt git avec ces deux fichiers dedans :)

[Dockerfile](./app/Dockerfile)
[Docker Compose](./app/docker-compose.yml)

```
git clone https://gitlab.com/marieliserenzema/flask-app.git
```
