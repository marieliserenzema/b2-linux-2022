# TP1 : (re)Familiaration avec un système GNU/Linux

## 0. Préparation de la machine

🌞 **Setup de deux machines Rocky Linux configurées de façon basique.**

- **un accès internet (via la carte NAT)**

node1
```
[ml@localhost ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=63 time=26.1 ms
```

node2 
```
[ml@localhost ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=63 time=49.0 ms
```

- **un accès à un réseau local**

node1 
```
[ml@localhost ~]$ cd /etc/sysconfig/network-scripts/
[ml@localhost network-scripts]$ cat ifcfg-enp0s8
DEVICE=enp0s8
NAME=enp0s8

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.101.1.11
NETMASK=255.255.255.0

GATEWAY=10.101.1.1
DNS1=1.1.1.1
[ml@localhost ~]$ sudo systemctl restart NetworkManager
[ml@localhost ~]$ ip a
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:ca:af:0c brd ff:ff:ff:ff:ff:ff
    inet 10.101.1.11/24 brd 10.101.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:feca:af0c/64 scope link
       valid_lft forever preferred_lft forever
[ml@localhost ~]$ ping 10.101.1.12
PING 10.101.1.12 (10.101.1.12) 56(84) bytes of data.
64 bytes from 10.101.1.12: icmp_seq=1 ttl=64 time=0.409 ms
64 bytes from 10.101.1.12: icmp_seq=2 ttl=64 time=1.04 ms
```

node2
```
[ml@localhost ~]$ ip a
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:4b:d7:59 brd ff:ff:ff:ff:ff:ff
    inet 10.101.1.12/24 brd 10.101.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe4b:d759/64 scope link
       valid_lft forever preferred_lft forever
[ml@localhost ~]$ ping 10.101.1.11
PING 10.101.1.11 (10.101.1.11) 56(84) bytes of data.
64 bytes from 10.101.1.11: icmp_seq=1 ttl=64 time=0.500 ms
```

- **les machines doivent avoir un nom**

node1
```
[ml@localhost ~]$ sudo nano /etc/hostname
[ml@localhost ~]$ cd /etc/
[ml@localhost etc]$ cat hostname
node1.tp1.b2
[ml@localhost etc]$ hostname
node1.tp1.b2
```

node2
```
[ml@node2 ~]$ hostname
node2.tp1.b2
```

- **utiliser `1.1.1.1` comme serveur DNS**

node1
```
[ml@localhost etc]$ sudo nano /etc/resolv.conf
[ml@localhost etc]$ cat /etc/resolv.conf
nameserver 1.1.1.1
[ml@localhost etc]$ dig ynov.com
;; ANSWER SECTION:
ynov.com.		300	IN	A	104.26.10.233
ynov.com.		300	IN	A	104.26.11.233
ynov.com.		300	IN	A	172.67.74.226

;; Query time: 44 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Mon Nov 14 15:10:24 CET 2022
;; MSG SIZE  rcvd: 85
```
node2 
```
[ml@node2 etc]$ dig ynov.com
;; ANSWER SECTION:
ynov.com.		300	IN	A	172.67.74.226
ynov.com.		300	IN	A	104.26.11.233
ynov.com.		300	IN	A	104.26.10.233

;; Query time: 144 msec
;; SERVER: 8.8.8.8#53(8.8.8.8)
;; WHEN: Mon Nov 14 15:12:29 CET 2022
;; MSG SIZE  rcvd: 85
```

- **les machines doivent pouvoir se joindre par leurs noms respectifs**

node1
```
[ml@localhost etc]$ cat hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
10.101.1.12 node2.tp1.b2
[ml@localhost etc]$ ping node2.tp1.b2
PING node2.tp1.b2 (10.101.1.12) 56(84) bytes of data.
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=1 ttl=64 time=0.748 ms
```

node2
```
[ml@node2 etc]$ ping node1
PING node1.tp1.b2 (10.101.1.11) 56(84) bytes of data.
64 bytes from node1.tp1.b2 (10.101.1.11): icmp_seq=1 ttl=64 time=0.341 ms
```

- **le pare-feu est configuré pour bloquer toutes les connexions exceptées celles qui sont nécessaires**

node1
```
[ml@localhost etc]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[ml@localhost ~]$ sudo firewall-cmd --remove-service cockpit
[sudo] password for ml:
success
[ml@localhost ~]$ sudo firewall-cmd --remove-service dhcpv6-client
success
[ml@localhost ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: ssh
  ports:
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

node2
```
[ml@node2 etc]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: ssh
  ports:
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

## I. Utilisateurs

### 1. Création et configuration

🌞 **Ajouter un utilisateur à la machine**, qui sera dédié à son administration

```
[ml@node1 ~]$ sudo useradd tata -m -s /bin/sh
[ml@node1 ~]$ cat /etc/passwd
tata:x:1001:1001::/home/tata:/bin/sh
```

```
[ml@node2 ~]$ cat /etc/passwd
toto:x:1001:1001::/home/toto:/bin/sh
```

🌞 **Créer un nouveau groupe `admins`** qui contiendra les utilisateurs de la machine ayant accès aux droits de `root` *via* la commande `sudo`.

```
[ml@node1 ~]$ sudo groupadd admins
[ml@node1 ~]$ sudo visudo /etc/sudoers
[ml@node1 ~]$ sudo cat /etc/sudoers
## Allows people in group wheel to run all commands
%admins ALL=(ALL) 	ALL
[ml@node1 ~]$ sudo cat /etc/group
admins:x:1002
```

```
[ml@node2 ~]$ cat /etc/group
admins:x:1002
```

🌞 **Ajouter votre utilisateur à ce groupe `admins`**

```
[ml@node1 ~]$ sudo usermod -aG admins tata
[ml@node1 ~]$ sudo cat /etc/group
admins:x:1002:tata
[ml@node1 ~]$ sudo passwd tata
Changing password for user tata.
passwd: all authentication tokens updated successfully.
[ml@node1 ~]$ su - tata
Password:
Last login: Mon Nov 14 16:24:26 CET 2022 on pts/0
[tata@node1 ~]$ sudo nano lol

We trust you have received the usual lecture from the local System Administrator
It usually boils down to these three things:

    #1) Respect the privacy of others.
    #2) Think before you type.
    #3) With great power comes great responsibility.
```

### 2. SSH

🌞 **Pour cela...**

```
MacBook-Pro-de-saperlipopette:~ marieliserenzema$ ssh-keygen -t rsa -b 4096
Generating public/private rsa key pair.
MacBook-Pro-de-saperlipopette:~ marieliserenzema$ ssh-copy-id ml@10.101.1.11
```

🌞 **Assurez vous que la connexion SSH est fonctionnelle**, sans avoir besoin de mot de passe.

```
MacBook-Pro-de-saperlipopette:~ marieliserenzema$ ssh ml@10.101.1.11
Last login: Mon Nov 14 16:32:00 2022 from 10.101.1.1
[ml@node1 ~]$
```

## II. Partitionnement

### 1. Préparation de la VM

### 2. Partitionnement

🌞 **Utilisez LVM** pour...

```
[ml@node1 ~]$ lsblk
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
sda           8:0    0    8G  0 disk
├─sda1        8:1    0    1G  0 part /boot
└─sda2        8:2    0    7G  0 part
  ├─rl-root 253:0    0  6.2G  0 lvm  /
  └─rl-swap 253:1    0  820M  0 lvm  [SWAP]
sdb           8:16   0    3G  0 disk
sdc           8:32   0    3G  0 disk
sr0          11:0    1 1024M  0 rom
sr1          11:1    1 1024M  0 rom
[ml@node1 ~]$ sudo pvcreate /dev/sdb
[sudo] password for ml:
  Physical volume "/dev/sdb" successfully created.
[ml@node1 ~]$ sudo pvcreate /dev/sdc
  Physical volume "/dev/sdc" successfully created.
[ml@node1 ~]$ sudo vgcreate data /dev/sdb
  Volume group "data" successfully created
[ml@node1 ~]$ sudo vgextend data /dev/sdc
  Volume group "data" successfully extended
[ml@node1 ~]$ udo lvcreate -L 1G data -n first_data
-bash: udo: command not found
[ml@node1 ~]$ sudo lvcreate -L 1G data -n first_data
  Logical volume "first_data" created.
[ml@node1 ~]$ sudo lvcreate -L 1G data -n second_data
  Logical volume "second_data" created.
[ml@node1 ~]$ sudo lvcreate -L 1G data -n third_data
  Logical volume "third_data" created.
[ml@node1 ~]$ mkfs -t ext4 /dev/data/first_data
mke2fs 1.46.5 (30-Dec-2021)
mkfs.ext4: Permission denied while trying to determine filesystem size
[ml@node1 ~]$ sudo mkfs -t ext4 /dev/data/first_data
mke2fs 1.46.5 (30-Dec-2021)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: 7910f1e3-4a50-4637-8f2c-a6a51a2492be
Superblock backups stored on blocks:
	32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done

[ml@node1 ~]$ sudo mkfs -t ext4 /dev/data/second_data
mke2fs 1.46.5 (30-Dec-2021)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: 3cdbd6f5-ed7e-40ac-b64d-e0c041ddd876
Superblock backups stored on blocks:
	32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done

[ml@node1 ~]$ sudo mkfs -t ext4 /dev/data/third_data
mke2fs 1.46.5 (30-Dec-2021)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: 4a1c1673-6c90-446c-a20f-9e931d06e220
Superblock backups stored on blocks:
	32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done

[ml@node1 ~]$ sudo mkdir /mnt/part1
[ml@node1 ~]$ sudo mkdir /mnt/part2
[ml@node1 ~]$ sudo mkdir /mnt/part3
[ml@node1 ~]$ sudo mount /dev/data/first_data /mnt/part1
[ml@node1 ~]$ sudo mount /dev/data/second_data /mnt/part2
[ml@node1 ~]$ sudo mount /dev/data/third_data /mnt/part3
[ml@node1 ~]$ mount
/dev/mapper/data-first_data on /mnt/part1 type ext4 (rw,relatime,seclabel)
/dev/mapper/data-second_data on /mnt/part2 type ext4 (rw,relatime,seclabel)
/dev/mapper/data-third_data on /mnt/part3 type ext4 (rw,relatime,seclabel)
```

🌞 **Grâce au fichier `/etc/fstab`**, faites en sorte que cette partition soit montée automatiquement au démarrage du système.

```
[ml@node1 ~]$ cat /etc/fstab
/dev/data/first_data /mnt/part1 ext4 defaults 0 0
/dev/data/second_data /mnt/part2 ext4 defaults 0 0
/dev/data/third_data /mnt/part3 ext4 defaults 0 0
```

## III. Gestion de services

## 1. Interaction avec un service existant

🌞 **Assurez-vous que...**

```
[ml@node1 ~]$ sudo systemctl status firewalld
● firewalld.service - firewalld - dynamic firewall daemon
     Loaded: loaded (/usr/lib/systemd/system/firewalld.service; enabled; vendor>
     Active: active (running) since Mon 2022-11-14 16:40:41 CET; 19min ago
       Docs: man:firewalld(1)
   Main PID: 657 (firewalld)
      Tasks: 2 (limit: 5907)
     Memory: 42.0M
        CPU: 493ms
     CGroup: /system.slice/firewalld.service
             └─657 /usr/bin/python3 -s /usr/sbin/firewalld --nofork --nopid
[ml@node1 system]$ sudo systemctl is-enabled firewalld
enabled
```

## 2. Création de service

### A. Unité simpliste

⚠️ **Uniquement sur `node1.tp1.b2`.**

🌞 **Créer un fichier qui définit une unité de service** 

```
[ml@node1 system]$ sudo nano
[ml@node1 system]$ cat web.service
[Unit]
Description=Very simple web service

[Service]
ExecStart=/usr/bin/python3 -m http.server 8888

[Install]
WantedBy=multi-user.target
[ml@node1 ~]$ sudo firewall-cmd --add-port=8888/tcp --permanent
success
[ml@node1 ~]$ sudo systemctl daemon-reload
[ml@node1 ~]$ sudo systemctl status web
○ web.service - Very simple web service
     Loaded: loaded (/etc/systemd/system/web.service; disabled; vendor preset: >
     Active: inactive (dead)
lines 1-3/3 (END)
^C
[ml@node1 ~]$ sudo systemctl start web
[ml@node1 ~]$ sudo systemctl enable web
Created symlink /etc/systemd/system/multi-user.target.wants/web.service → /etc/systemd/system/web.service.
[ml@node1 ~]$ sudo systemctl status web
● web.service - Very simple web service
     Loaded: loaded (/etc/systemd/system/web.service; enabled; vendor preset: d>
     Active: active (running) since Mon 2022-11-14 17:06:32 CET; 13s ago
   Main PID: 1144 (python3)
      Tasks: 1 (limit: 5907)
     Memory: 9.3M
        CPU: 63ms
     CGroup: /system.slice/web.service
             └─1144 /usr/bin/python3 -m http.server 8888

Nov 14 17:06:32 node1.tp1.b2 systemd[1]: Started Very simple web service.
lines 1-11/11 (END)
```

🌞 **Une fois le service démarré, assurez-vous que pouvez accéder au serveur web**

```
[ml@node1 ~]$ curl 10.101.1.11:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="afs/">afs/</a></li>
<li><a href="bin/">bin@</a></li>
<li><a href="boot/">boot/</a></li>
<li><a href="dev/">dev/</a></li>
<li><a href="etc/">etc/</a></li>
<li><a href="home/">home/</a></li>
<li><a href="lib/">lib@</a></li>
<li><a href="lib64/">lib64@</a></li>
<li><a href="media/">media/</a></li>
<li><a href="mnt/">mnt/</a></li>
<li><a href="opt/">opt/</a></li>
<li><a href="proc/">proc/</a></li>
<li><a href="root/">root/</a></li>
<li><a href="run/">run/</a></li>
<li><a href="sbin/">sbin@</a></li>
<li><a href="srv/">srv/</a></li>
<li><a href="sys/">sys/</a></li>
<li><a href="tmp/">tmp/</a></li>
<li><a href="usr/">usr/</a></li>
<li><a href="var/">var/</a></li>
</ul>
<hr>
</body>
</html>
```

### B. Modification de l'unité

🌞 **Préparez l'environnement pour exécuter le mini serveur web Python**


```
[ml@node1 ~]$ sudo useradd web -m -s /bin/sh
[ml@node1 ~]$ sudo usermod -aG admins web
[ml@node1 ~]$ sudo passwd web
Changing password for user web.
passwd: all authentication tokens updated successfully.
[ml@node1 ~]$ su - web
[web@node1 ~]$ cd /var/www/meow/
[web@node1 meow]$ sudo nano waaf
[web@node1 /]$ sudo chown web var/www/meow/waaf
[web@node1 /]$ ls -al /var/www/meow/waaf
-rw-r--r--. 1 web root 5 Nov 14 17:22 /var/www/meow/waaf
[web@node1 ~]$ cat /var/www/meow/waaf
WAAF
```

🌞 **Modifiez l'unité de service `web.service` créée précédemment en ajoutant les clauses**

```
[web@node1 system]$ cat web.service
[Unit]
Description=Very simple web service

[Service]
ExecStart=/usr/bin/python3 -m http.server 8888
User=web
WorkingDirectory=/var/www/meow/

[Install]
WantedBy=multi-user.target
[web@node1 ~]$ sudo systemctl stop web
[web@node1 ~]$ sudo systemctl start web
```


🌞 **Vérifiez le bon fonctionnement avec une commande `curl`**

```
[web@node1 ~]$ curl localhost:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="waaf">waaf</a></li>
</ul>
<hr>
</body>
</html>
```