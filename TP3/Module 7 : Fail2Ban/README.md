# TP3 : Amélioration de la solution NextCloud

# Module 7 : Fail2Ban

➜ **Installation**

```
[ml@db ~]$ sudo dnf -y install fail2ban
Complete!
```

- Démarrage du service 

```
[ml@db ~]$ sudo systemctl start fail2ban
[ml@db ~]$ sudo systemctl enable fail2ban
Created symlink /etc/systemd/system/multi-user.target.wants/fail2ban.service → /usr/lib/systemd/system/fail2ban.service.
[ml@db ~]$ sudo systemctl status fail2ban
● fail2ban.service - Fail2Ban Service
     Loaded: loaded (/usr/lib/systemd/system/fail2ban.service; enabled; vendor >
     Active: active (running) since Mon 2022-11-21 23:39:58 CET; 12s ago
       Docs: man:fail2ban(1)
   Main PID: 2778 (fail2ban-server)
      Tasks: 3 (limit: 5907)
     Memory: 10.3M
        CPU: 79ms
     CGroup: /system.slice/fail2ban.service
             └─2778 /usr/bin/python3 -s /usr/bin/fail2ban-server -xf start
[ml@db ~]$ sudo systemctl is-enabled fail2ban
enabled
```

➜ **Configuration**

```
[ml@db ~]$ sudo nano /etc/fail2ban/jail.d/custom.conf
[DEFAULT]
findtime = 1m
bantime = 24h
maxretry = 3

[sshd]
enabled = true
maxretry = 3
findtime = 1m
bantime = 24h
```

```
[ml@db ~]$ sudo systemctl reload fail2ban
```

➜ **Test**

```
[ml@web ~]$ ssh ml@10.102.1.12
ml@10.102.1.12's password:
Permission denied, please try again.
ml@10.102.1.12's password:
Permission denied, please try again.
ml@10.102.1.12's password:
ml@10.102.1.12: Permission denied (publickey,gssapi-keyex,gssapi-with-mic,password).
```

