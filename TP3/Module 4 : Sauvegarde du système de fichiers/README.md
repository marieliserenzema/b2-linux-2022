# TP3 : Amélioration de la solution NextCloud

# Module 4 : Sauvegarde du système de fichiers

## I. Script de backup

### 1. Ecriture du script

➜ **Ecrire le script `bash`**

![Script Bash](./tp3_backup.sh)

### 2. Clean it

➜ **Environnement d'exécution du script**

- Création d'un user : Cet utilisateur sera celui qui lancera le script

```
[ml@web ~]$ sudo useradd backup -m -d /srv/backup/ -s /usr/bin/nologin
[ml@web ~]$ sudo chown backup /srv/tp3_backup.sh
[ml@web ~]$ sudo chown backup /srv/backup
```

### 3. Service et timer

➜ **Créez un service système qui lance le script**

```
[ml@web ~]$ sudo -u backup chmod 500 /srv/tp3_backup.sh
[ml@web ~]$ sudo nano /etc/systemd/system/backup.service
[Unit]
Description=My Shell Script

[Service]
ExecStart=/bin/bash /srv/tp3_backup.sh
Type=oneshot
User=backup

[Install]
WantedBy=multi-user.target
```

- Démarrage du service 

```
[ml@web ~]$ sudo systemctl daemon-reload
[ml@web ~]$ sudo systemctl start backup
[ml@web ~]$ sudo systemctl status backup
```

➜ **Créez un timer système qui lance le service à intervalles réguliers**

```
[ml@web ~]$ sudo nano /etc/systemd/system/backup.timer
[Unit]
Description=Run service X

[Timer]
OnCalendar=*-*-* 4:00:00

[Install]
WantedBy=timers.target
[ml@web ~]$ sudo systemctl daemon-reload
```

- Démarrage du service 

```
[ml@web ~]$ sudo systemctl start backup.timer
[ml@web ~]$ sudo systemctl enable backup.timer
Created symlink /etc/systemd/system/timers.target.wants/backup.timer → /etc/systemd/system/backup.timer.
[ml@web ~]$ sudo systemctl status backup.timer
● backup.timer - Run service X
     Loaded: loaded (/etc/systemd/system/backup.timer; enabled; vendor preset: disabled)
     Active: active (waiting) since Mon 2022-11-21 23:07:10 CET; 13s ago
      Until: Mon 2022-11-21 23:07:10 CET; 13s ago
    Trigger: Tue 2022-11-22 04:00:00 CET; 4h 52min left
   Triggers: ● backup.service

Nov 21 23:07:10 web.tp2.linux systemd[1]: Started Run service X.
[ml@web ~]$ sudo systemctl is-enabled backup.timer
enabled
[ml@web ~]$ sudo systemctl list-timers
NEXT                        LEFT          LAST                        PASSED    UNIT                         ACTIVATES
Tue 2022-11-22 00:00:00 CET 51min left    Mon 2022-11-21 16:35:49 CET 6h ago    logrotate.timer              logrotate.service
Tue 2022-11-22 00:15:10 CET 1h 7min left  Mon 2022-11-21 22:40:41 CET 27min ago dnf-makecache.timer          dnf-makecache.service
Tue 2022-11-22 04:00:00 CET 4h 51min left n/a                         n/a       backup.timer                 backup.service
Tue 2022-11-22 22:28:34 CET 23h left      Mon 2022-11-21 22:28:34 CET 39min ago systemd-tmpfiles-clean.timer systemd-tmpfiles-clean.service

4 timers listed.
Pass --all to see loaded but inactive timers, too.
```


## II. NFS

### 1. Serveur NFS

🖥️ **VM `storage.tp3.linux`**

➜ **Préparer un dossier à partager** sur le réseaucsur la machine `storage.tp3.linux`

```
[ml@storage ~]$ sudo mkdir /srv/nfs_shares
[ml@storage ~]$ sudo mkdir /srv/nfs_shares/web.tp2.linux/
```

➜ **Installer le serveur NFS**

```
[ml@storage ~]$ sudo dnf -y install nfs-utils
```

```
[ml@storage ~]$ sudo nano /etc/exports
/srv/nfs_shares/web.tp2.linux  	  web.tp2.linux
```


```
[ml@storage ~]$ sudo firewall-cmd --permanent --add-service=nfs
success
[ml@storage ~]$ sudo firewall-cmd --permanent --add-service=mountd
success
[ml@storage ~]$ sudo firewall-cmd --permanent --add-service=rpc-bind
success
[ml@storage ~]$ sudo firewall-cmd --reload
success
```
- Démarrage du service 

```
[ml@storage ~]$ sudo systemctl enable nfs-server
Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.
[ml@storage ~]$ sudo systemctl start nfs-server
[ml@storage ~]$ sudo systemctl status nfs-server
● nfs-server.service - NFS server and services
     Loaded: loaded (/usr/lib/systemd/system/nfs-server.service; enabled; vendo>
    Drop-In: /run/systemd/generator/nfs-server.service.d
             └─order-with-mounts.conf
     Active: active (exited) since Tue 2022-11-22 12:12:07 CET; 4s ago
    Process: 1662 ExecStartPre=/usr/sbin/exportfs -r (code=exited, status=0/SUC>
    Process: 1663 ExecStart=/usr/sbin/rpc.nfsd (code=exited, status=0/SUCCESS)
    Process: 1680 ExecStart=/bin/sh -c if systemctl -q is-active gssproxy; then>
   Main PID: 1680 (code=exited, status=0/SUCCESS)
        CPU: 17ms
[ml@storage ~]$ sudo systemctl is-enabled nfs-server
enabled
```

### 2. CLient NFS

➜ **Installer un client NFS sur ```web.tp2.linux```**

```
[ml@web ~]$ sudo dnf install nfs-utils
Complete!
```

- Démarrage du service 

```
[ml@web ~]$ sudo systemctl enable nfs-server
Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.
[ml@web ~]$ sudo systemctl start nfs-server
[ml@web ~]$ sudo systemctl status nfs-server
● nfs-server.service - NFS server and services
     Loaded: loaded (/usr/lib/systemd/system/nfs-server.service; enabled; vendo>
     Active: active (exited) since Tue 2022-11-22 12:31:13 CET; 5s ago
    Process: 2308 ExecStartPre=/usr/sbin/exportfs -r (code=exited, status=0/SUC>
    Process: 2309 ExecStart=/usr/sbin/rpc.nfsd (code=exited, status=0/SUCCESS)
    Process: 2326 ExecStart=/bin/sh -c if systemctl -q is-active gssproxy; then>
   Main PID: 2326 (code=exited, status=0/SUCCESS)
        CPU: 17ms
[ml@web ~]$ sudo systemctl is-enabled nfs-server
enabled
```

```
[ml@web ~]$ sudo firewall-cmd --permanent --add-service=nfs
success
[ml@web ~]$ sudo firewall-cmd --permanent --add-service=mountd
success
[ml@web ~]$ sudo firewall-cmd --permanent --add-service=rpc-bind
success
[ml@web ~]$ sudo firewall-cmd --reload
success
```

```
[ml@web ~]$ sudo mount storage:/srv/nfs_shares/web.tp2.linux /srv/backup
[ml@web ~]$ sudo nano /etc/fstab
storage:/srv/nfs_shares/web.tp2.linux   /srv/backup  nfs auto,nofail,noatime,nolock,intr,tcp,actimeo=1800 0 0
[ml@web ~]$ df -h
Filesystem                             Size  Used Avail Use% Mounted on
storage:/srv/nfs_shares/web.tp2.linux  6.2G  1.2G  5.1G  18% /srv/backup
[ml@web ~]$ sudo touch /srv/backup/test
```

```
[ml@storage ~]$ cd /srv/nfs_shares/web.tp2.linux/
[ml@storage web.tp2.linux]$ ls
test
```

➜ **Tester la restauration des données**

```
[ml@web ~]$ sudo -u backup unzip /srv/backup/nextcloud_221123032244.zip
[ml@web ~]$ sudo -u backup rsync -Aax /srv/backup/var/ /var/www/tp2_nextcloud
```






