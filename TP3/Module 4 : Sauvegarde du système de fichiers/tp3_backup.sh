# !/bin/bash

# Date : 2022/11/17
# Author : ehmelh
# Backup MariaDB database

# Variable

CONFIG_FOLDER='/var/www/tp2_nextcloud/config/'
DATA_FOLDER='/var/www/tp2_nextcloud/data/'
THEMES_FOLDER='/var/www/tp2_nextcloud/themes/'
DATABASE_FOLDER='/var/www/tp2_nextcloud/apps/'
BACKUP_DIR='/srv/backup'
DATE="$(date +%y%m%d%H%M%S)"
BACKUP_FILENAME="nextcloud_$DATE"

# Creates the directory
mkdir -p "$BACKUP_DIR"

# Install zip command
if sudo  dnf -y install zip;then
        echo "Zip succesfully installed"
else
    	echo "Problem to install zip command"
     exit 1
fi

# Do the backup
if zip "$BACKUP_DIR"/"$BACKUP_FILENAME".zip  $CONFIG_FOLDER $DATA_FOLDER $THEMES_FOLDER $DATABASE_FOLDER;then
     echo "Backup succesfully done"
else
     echo "Problem to backup database"
fi