# TP3 : Amélioration de la solution NextCloud

# Module 6 : Automatiser le déploiement

➜ **Le script `bash` pour pour déployer un NextCloud et sa base de données de façon automatisée**

- Insérer ses deux fichier sur votre VM

![Script Bash](./tp3_automation_nextcloud.sh)
![Fichier conf](./tp2.conf)

➜ **Le script `bash` qui automatise l'installation de la base de données de NextCloud**

![Script Bash](./tp3_automation_db.sh)