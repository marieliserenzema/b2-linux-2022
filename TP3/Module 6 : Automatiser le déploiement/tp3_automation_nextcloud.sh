#!/bin/bash

# Date : 2022/11/18
# Author : ehmelh
# Automate NextCloud deployement and its database

# Installation Apache
if dnf -y install httpd; then
	echo "Apache sucessfully installed"
else
	echo "Error with Apache installation"
	exit 1
fi

# Start Apache
if systemctl start httpd; then
	echo "Service sucessfully started"
else
	echo "Error starting service"
	exit 1
fi

if systemctl enable httpd; then
	echo "Service enabled"
else
	echo "Can't enabled service"
	exit 1
fi

# Adjusting firewall
if firewall-cmd --add-port=80/tcp --permanent; then
	echo "Port 80 sucessfully opened"
else
	echo "Problem to open port 80"
	exit 1
fi
firewall-cmd --reload

# Installation mySQL
if dnf -y install mysql-8.0.30-3.el9_0.x86_64;then
	echo "MySQL successfully installed"
else
	echo "Problem to install my SQL"
	exit 1
fi

# Installation PHP
dnf config-manager --set-enabled crb
dnf install dnf-utils http://rpms.remirepo.net/enterprise/remi-release-9.rpm -y
dnf module list php
dnf module enable php:remi-8.1 -y
dnf install -y php81-php

# Installation PHP moduls
if dnf install -y libxml2 openssl php81-php php81-php-ctype php81-php-curl php81-php-gd php81-php-iconv php81-php-json php81-php-libxml php81-php-mbstring php81-php-openssl php81-php-posix php81-php-session php81-php-xml php81-php-zip php81-php-zlib php81-php-pdo php81-php-mysqlnd php81-php-intl php81-php-bcmath php81-php-gmp; then
	echo "PHP moduls succesfully installed"
else
	echo "Problem to install PHP moduls"
	exit 1
fi

# Create directory
mkdir /var/www/tp2_nextcloud

# Download NextCloud
if curl -SLO https://download.nextcloud.com/server/prereleases/nextcloud-25.0.0rc3.zip;then
	echo "NextCloud sucessfully installed"
else
	echo "Problem to install NextCloud"
	exit 1
fi

# Installation unzip 
if dnf -y install unzip; then
	echo "Unzip successfully installed"
else
	echo "Problem to install unzip"
	exit 1
fi

# Unzip NextCloud directory
unzip nextcloud-25.0.0rc3.zip
mv nextcloud/* /var/www/tp2_nextcloud/

# Change directory permission
chown apache:apache /var/www/tp2_nextcloud/*

# Change Apache configuration
mv tp2.conf  /etc/httpd/conf.d/tp2.conf

# Restart Apache service
systemctl restart httpd

echo "Now you can connect to  NextCloud "
echo "Example of command : mysql -u nextcloud -h 10.102.1.14 -p"