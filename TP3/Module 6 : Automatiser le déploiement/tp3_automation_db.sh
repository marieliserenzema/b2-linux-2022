#!/bin/bash

# Date : 2022/11/21
# Author : ehmelh
# Automate installation of Mariadb and create a database

# Variables
USER="nextcloud"
IPADDR="10.102.1.14"
PASSWD="pewpewpew"
DATABASE="nextcloud"

# Installation of MariaDb
if dnf module install mariadb -y;then
    echo "MariaDB succesfully installed"
else 
    echo "Error to install MariaDB"
    exit 1
fi

# Start MariaDb
if systemctl start mariadb;then
    echo "MariaDB succesfully started"
else 
    echo "Error to start MariaDB"
    exit 1
fi

if systemctl enable mariadb;then
    echo "MariaDB succesfully enabled"
else 
    echo "Error to enbale MariaDB"
    exit 1
fi

# Adjusting the firewall
if firewall-cmd --add-port=3306/tcp --permanent;then
    echo "Port succesfully opened"
else 
    echo "Error to open port"
    exit 1
fi
firewall-cmd --reload


echo "Creating user ..."
mysql -e "CREATE USER "$USER"@"$IPADDR" IDENTIFIED BY "$PASSWD";"

echo "Creating database ..."
mysql -e "CREATE DATABASE IF NOT EXISTS "$DATABASE" CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;"

echo  "Granting all permissions to user on database ..."
mysql -e "GRANT ALL PRIVILEGES ON "$DATABASE".* TO "$USER"@"$IPADDR" ;""
mysql -e "FLUSH PRIVILEGES;"