# TP3 : Amélioration de la solution NextCloud

# Module 1 : Reverse Proxy

# I. Setup

➜ **Installation**

```
[ml@proxy ~]$ sudo dnf install nginx
Complete!
```

- Démarrage du service 

```
[ml@proxy ~]$ sudo systemctl start nginx
[ml@proxy ~]$ sudo systemctl enable nginx
Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.
[ml@proxy ~]$ sudo systemctl is-enabled nginx
enabled
[ml@proxy ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; vendor pre>
     Active: active (running) since Thu 2022-11-17 12:47:30 CET; 20s ago
   Main PID: 1024 (nginx)
      Tasks: 2 (limit: 5907)
     Memory: 1.9M
        CPU: 19ms
     CGroup: /system.slice/nginx.service
             ├─1024 "nginx: master process /usr/sbin/nginx"
             └─1025 "nginx: worker process"
[ml@proxy ~]$ sudo ss -ltpn | grep -w 'nginx'
LISTEN 0      511          0.0.0.0:80        0.0.0.0:*    users:(("nginx",pid=1025,fd=6),("nginx",pid=1024,fd=6))
LISTEN 0      511             [::]:80           [::]:*    users:(("nginx",pid=1025,fd=7),("nginx",pid=1024,fd=7))
```

```
[ml@proxy ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[ml@proxy ~]$ sudo firewall-cmd --reload
success
```
- Check de l'utilisateur qu'utilise nginx

```
[ml@proxy ~]$ sudo ps -ef | grep -w 'nginx'
root        1024       1  0 12:47 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx       1025    1024  0 12:47 ?        00:00:00 nginx: worker process
ml          1082     830  0 12:51 pts/0    00:00:00 grep --color=auto -w nginx
[ml@proxy ~]$ curl localhost:80
<!doctype html>
<html>
</html>
```

➜ **Configuration de NGINX**

- Modifier la conf de NGINX

```
[ml@proxy ~]$ sudo nano /etc/nginx/conf.d/tp3.conf
server {
    # On indique le nom que client va saisir pour accéder au service
    # Pas d'erreur ici, c'est bien le nom de web, et pas de proxy qu'on veut ici !
    server_name web.tp2.linux;

    # Port d'écoute de NGINX
    listen 80;

    location / {
        # On définit des headers HTTP pour que le proxying se passe bien
        proxy_set_header  Host $host;
        proxy_set_header  X-Real-IP $remote_addr;
        proxy_set_header  X-Forwarded-Proto https;
        proxy_set_header  X-Forwarded-Host $remote_addr;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;

        # On définit la cible du proxying
        proxy_pass http://web.tp2.linux:80;
    }

    # Deux sections location recommandés par la doc NextCloud
    location /.well-known/carddav {
      return 301 $scheme://$host/remote.php/dav;
    }

    location /.well-known/caldav {
      return 301 $scheme://$host/remote.php/dav;
    }
}
```
- Avertir NextCloud que l'on a ajouter un reverse proxy

```
[ml@web ~]$ sudo nano /var/www/tp2_nextcloud/config/config.php
<?php
<?php
$CONFIG = array (
  'instanceid' => 'oc5o2og5iy6f',
  'passwordsalt' => 'roxl5K1k9ypMvq2IAI2gNXezTdqa/u',
  'secret' => 'DOTvpn9TH9WviK8qWhEYYVWEvztH4CqWjGgYMt82Fi9Yn65V',
  'trusted_domains' =>
  array (
    0 => 'web.tp2.linux',
  ),
  'datadirectory' => '/var/www/tp2_nextcloud/data',
  'dbtype' => 'mysql',
  'version' => '25.0.0.15',
  'overwrite.cli.url' => 'http://web.tp2.linux',
  'dbname' => 'nextcloud',
  'dbhost' => '10.102.1.12:3306',
  'dbport' => '',
  'dbtableprefix' => 'oc_',
  'mysql.utf8mb4' => true,
  'dbuser' => 'nextcloud',
  'dbpassword' => 'pewpewpew',
  'installed' => true,
  'trusted_proxies' =>
  array (
   0 => '10.102.1.13',
  ),
  'overwritehost' => 'web.tp2.linux',
  'overwriteprotocol' => 'http',
);
```

➜ **Modifier le fichier ```hosts```de mon PC**

```
monpc:~ marieliserenzema$ sudo nano /etc/hosts
##
# Host Database
#
10.102.1.13 web.tp2.linux web 
```

➜ **Désactiver l'accès au serveur sauf pour le proxy**

```
[ml@web ~]$ sudo firewall-cmd --zone=internal --add-port=80/tcp --permanent
success
[ml@web ~]$ sudo firewall-cmd --zone=internal --add-source=10.102.1.13 --permanent
success
[ml@web ~]$ sudo firewall-cmd --zone=public --remove-port=80/tcp --permanent
success
[ml@web ~]$ sudo firewall-cmd --reload
success
```
```
MacBook-Pro-de-saperlipopette:~ marieliserenzema$ curl 10.102.1.11
curl: (7) Failed to connect to 10.102.1.11 port 80 after 10 ms: Connection refused
```

# II. HTTPS

➜ **Générer une paire de clés**

```
[ml@proxy ~]$ sudo dnf install openssl
```

```
[ml@proxy ~]$ openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout server.key -out server.crt
Common Name (eg, your name or your server's hostname) []:web.tp2.linux
[ml@proxy ~]$ ls
server.crt  server.key
```

```
[ml@proxy ~]$ mv server.crt web.tp2.linux.crt
[ml@proxy ~]$ mv server.key web.tp2.linux.key
[ml@proxy ~]$ sudo mv web.tp2.linux.* /srv
[ml@proxy private]$ sudo mv /srv/web.tp2.linux.key /etc/pki/tls/private/
[ml@proxy private]$ sudo mv /srv/web.tp2.linux.crt /etc/pki/tls/certs/
[ml@proxy ~]$ sudo cat /etc/nginx/conf.d/tp3.conf
server {
    # On indique le nom que client va saisir pour accéder au service
    # Pas d'erreur ici, c'est bien le nom de web, et pas de proxy qu'on veut ici !
    server_name web.tp2.linux;

    # Port d'écoute de NGINX
    listen 443 ssl;

    ssl_certificate         /etc/pki/tls/certs/web.tp2.linux.crt;
    ssl_certificate_key     /etc/pki/tls/private/web.tp2.linux.key;

    location / {
        # On définit des headers HTTP pour que le proxying se passe bien
        proxy_set_header  Host $host;
        proxy_set_header  X-Real-IP $remote_addr;
        proxy_set_header  X-Forwarded-Proto https;
        proxy_set_header  X-Forwarded-Host $remote_addr;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;

        # On définit la cible du proxying
        proxy_pass http://web.tp2.linux:80;
    }

    # Deux sections location recommandés par la doc NextCloud
    location /.well-known/carddav {
      return 301 $scheme://$host/remote.php/dav;
    }

    location /.well-known/caldav {
      return 301 $scheme://$host/remote.php/dav;
    }

}
server {
    listen 80 default_server;
    listen [::]:80 default_server;
    return 301 https://$host$request_uri;
}
[ml@proxy ~]$ sudo systemctl reload nginx
```

```
[ml@proxy ~]$ sudo firewall-cmd --add-port=443/tcp --permanent
success
[ml@proxy ~]$ sudo firewall-cmd --reload
success
```