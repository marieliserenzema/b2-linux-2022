# TP3 : Amélioration de la solution NextCloud

# Module 5 : Monitoring

➜ **Mise en place de NetData**

- Installation 

```
[ml@web ~]$ wget -O /tmp/netdata-kickstart.sh https://my-netdata.io/kickstart.sh && sh /tmp/netdata-kickstart.sh
Successfully installed the Netdata Agent.
```

- Start NetData

```
[ml@web ~]$ sudo systemctl start netdata
[ml@web ~]$ sudo systemctl enable netdata
[ml@web ~]$ sudo systemctl status netdata
● netdata.service - Real time performance monitoring
     Loaded: loaded (/usr/lib/systemd/system/netdata.service; enabled; vendor p>
     Active: active (running) since Fri 2022-11-18 15:21:59 CET; 1min 12s ago
   Main PID: 1722 (netdata)
      Tasks: 56 (limit: 5907)
     Memory: 67.3M
        CPU: 2.453s
     CGroup: /system.slice/netdata.service
             ├─1722 /usr/sbin/netdata -P /run/netdata/netdata.pid -D
             ├─1724 /usr/sbin/netdata --special-spawn-server
             ├─1923 bash /usr/libexec/netdata/plugins.d/tc-qos-helper.sh 1
             ├─1939 /usr/libexec/netdata/plugins.d/apps.plugin 1
             └─1943 /usr/libexec/netdata/plugins.d/go.d.plugin 1
```

- Configurer le firewall

```
[ml@web ~]$ sudo firewall-cmd --permanent --add-port=19999/tcp
success
[ml@web ~]$ sudo firewall-cmd --reload
success
```

➜ **Configurer Netdata pour qu'il vous envoie des alertes**

```
[ml@web ~]$ sudo nano /etc/netdata/health_alarm_notify.conf
###############################################################################
# sending discord notifications

# note: multiple recipients can be given like this:
#                  "CHANNEL1 CHANNEL2 ..."

# enable/disable sending discord notifications
SEND_DISCORD="YES"

# Create a webhook by following the official documentation -
# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/1043178147832664084/pEuV_dudqg25txrpNQBPNtrqFL2wqaxDX4JPuOluW551wZ7HwgUw-xrJvunNktgIUmae"

# if a role's recipients are not configured, a notification will be send to
# this discord channel (empty = do not send a notification for unconfigured
# roles):
DEFAULT_RECIPIENT_DISCORD="alarms"
```

➜ **Vérifier que les alertes fonctionnent**
 
```
ml@web ~]$ sudo /usr/libexec/netdata/plugins.d/alarm-notify.sh test

# SENDING TEST WARNING ALARM TO ROLE: sysadmin
2022-11-18 15:57:14: alarm-notify.sh: INFO: sent discord notification for: web.tp2.linux test.chart.test_alarm is WARNING to 'web-tp2-linux'
# OK

# SENDING TEST CRITICAL ALARM TO ROLE: sysadmin
2022-11-18 15:57:14: alarm-notify.sh: INFO: sent discord notification for: web.tp2.linux test.chart.test_alarm is CRITICAL to 'web-tp2-linux'
# OK

# SENDING TEST CLEAR ALARM TO ROLE: sysadmin
2022-11-18 15:57:15: alarm-notify.sh: INFO: sent discord notification for: web.tp2.linux test.chart.test_alarm is CLEAR to 'web-tp2-linux'
# OK
[ml@web ~]$ sudo nano /etc/netdata/health_alarm_notify.conf
[ml@web ~]$ sudo /usr/libexec/netdata/plugins.d/alarm-notify.sh test

# SENDING TEST WARNING ALARM TO ROLE: sysadmin
2022-11-18 15:58:18: alarm-notify.sh: INFO: sent discord notification for: web.tp2.linux test.chart.test_alarm is WARNING to 'web-tp2-linux'
# OK

# SENDING TEST CRITICAL ALARM TO ROLE: sysadmin
2022-11-18 15:58:19: alarm-notify.sh: INFO: sent discord notification for: web.tp2.linux test.chart.test_alarm is CRITICAL to 'web-tp2-linux'
# OK

# SENDING TEST CLEAR ALARM TO ROLE: sysadmin
2022-11-18 15:58:19: alarm-notify.sh: INFO: sent discord notification for: web.tp2.linux test.chart.test_alarm is CLEAR to 'web-tp2-linux'
# OK
```

J'ai fais la même chose sur la machine ```db.tp2.linux``` sauf que pour les alertes sont redirigées vers un autre salon dans mon serveur discord
pour que mes 2 machines aient chacunes un salon.