# !/bin/bash

# Date : 2022/11/17
# AUthor : ehmelh
# Retrieves content from the database used by NextCloud

source /srv/db_pass

# User & password configuration
DB_USER="admin"
DB_PASS="$var"

# Host configuration
DB_HOST="localhost"
DB_NAME="nextcloud"

# Creates the directory
mkdir -p /srv/db_dumps/

# retrieves the date infos
DATE=`date +%y%m%d%H%M%S`

# creates the filename
FILENAME="db_${DB_NAME}_${DATE}"

mysqldump -h $DB_HOST -u $DB_USER -p$DB_PASS $DB_NAME > /srv/db_dumps/$FILENAME.sql

# compress the file
zip /srv/db_dumps/$FILENAME.zip /srv/db_dumps/$FILENAME.sql

# removes the file
rm /srv/db_dumps/$FILENAME.sql
