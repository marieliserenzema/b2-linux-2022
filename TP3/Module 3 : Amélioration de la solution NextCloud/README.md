# TP3 : Amélioration de la solution NextCloud

# Module 3 : Sauvegarde de base de données

➜ **Créer un utilisateur DANS LA BASE DE DONNEES**

```
[ml@db ~]$  sudo mysql -u root -p
```
```sql
MariaDB [(none)]> CREATE USER 'admin'@localhost IDENTIFIED BY 'toto';
MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'admin'@localhost;
MariaDB [(none)]> FLUSH PRIVILEGES;
MariaDB [(none)]> SHOW GRANTS FOR 'admin'@localhost;
+--------------------------------------------------------------------------------------------------------------+
| Grants for admin@localhost                                                                                   |
+--------------------------------------------------------------------------------------------------------------+
| GRANT USAGE ON *.* TO `admin`@`localhost` IDENTIFIED BY PASSWORD '*63D85DCA15EAFFC58C908FD2FAE50CCBC60C4EA2' |
| GRANT ALL PRIVILEGES ON `nextcloud`.* TO `admin`@`localhost`                                                 |
+--------------------------------------------------------------------------------------------------------------+
```

## I. Script dump

➜ **Ecrire le script `bash`**

![Script Bash](./tp3_db_dump.sh)

➜ **Création d'un user**

Cet utilisateur sera celui qui lancera le script

```
[ml@db ~]$ sudo useradd db_dumps -m -d /srv/db_dumps/ -s /usr/bin/nologin
[ml@db ~]$ ls -al /srv/db_dumps/
total 56
drwxr-xr-x. 2 db_dumps root        80 Nov 18 15:04 .
drwxr-xr-x. 3 db_dumps root        59 Nov 17 22:55 ..
-rw-r--r--. 1 db_dumps db_dumps 26126 Nov 18 14:49 db_localhost_221118144956.zip
-rw-r--r--. 1 db_dumps db_dumps 26125 Nov 18 15:04 db_localhost_221118150454.zip
[ml@db ~]$ sudo -u db_dumps /srv/tp3_db_dump.sh
  adding: srv/db_dumps/db_localhost_221118150633.sql (deflated 83%)
```
➜ **Stocker le mot de passe pour se co à la base dans un fichier séparé**

![MDP](./db_pass)


## II. Service et timer

➜ **Créez un service système qui lance le script**

```
[ml@db ~]$ sudo cat /etc/systemd/system/db-dump.service
[Unit]
Description=My Shell Script

[Service]
ExecStart=/bin/bash /srv/tp3_db_dump.sh
Type=oneshot
User=db_dumps

[Install]
WantedBy=multi-user.target
```

```
[ml@db ~]$ sudo systemctl daemon-reload
```

- Démarrage du service 

```
[ml@db ~]$ sudo systemctl start db-dump
[ml@db ~]$ sudo systemctl status db-dump
○ db-dump.service - My Shell Script
     Loaded: loaded (/etc/systemd/system/db-dump.service; disabled; vendor preset: disabled)
     Active: inactive (dead) since Fri 2022-11-18 15:10:23 CET; 8s ago
TriggeredBy: ● db-dump.timer
    Process: 1121 ExecStart=/bin/bash /srv/tp3_db_dump.sh (code=exited, status=0/SUCCESS)
   Main PID: 1121 (code=exited, status=0/SUCCESS)
        CPU: 28ms

Nov 18 15:10:23 db.tp2.linux systemd[1]: Starting My Shell Script...
Nov 18 15:10:23 db.tp2.linux bash[1125]:   adding: srv/db_dumps/db_localhost_221118151023.sql (deflated 83%)
Nov 18 15:10:23 db.tp2.linux systemd[1]: db-dump.service: Deactivated successfully.
Nov 18 15:10:23 db.tp2.linux systemd[1]: Finished My Shell Script.
[ml@db ~]$ sudo systemctl enable db-dump
Created symlink /etc/systemd/system/multi-user.target.wants/db-dump.service → /etc/systemd/system/db-dump.service.
[ml@db ~]$ sudo systemctl is-enabled db-dump
enabled
```
➜ **Créez un timer système qui lance le service à intervalles réguliers**

```
[ml@db ~]$ sudo cat /etc/systemd/system/db-dump.timer
[Unit]
Description=Run service X

[Timer]
OnCalendar=*-*-* 4:00:00

[Install]
WantedBy=timers.target
[ml@db ~]$ sudo systemctl daemon-reload
```

- Démarrage du service 

```
[ml@db ~]$ sudo systemctl start db-dump.timer
[ml@db ~]$ sudo systemctl enable db-dump.timer
[ml@db ~]$ sudo systemctl status db-dump.timer
● db-dump.timer - Run service X
     Loaded: loaded (/etc/systemd/system/db-dump.timer; enabled; vendor preset: disabled)
     Active: active (waiting) since Fri 2022-11-18 14:54:41 CET; 18min ago
      Until: Fri 2022-11-18 14:54:41 CET; 18min ago
    Trigger: Sat 2022-11-19 04:00:00 CET; 12h left
   Triggers: ● db-dump.service

Nov 18 14:54:41 db.tp2.linux systemd[1]: Started Run service X.
[ml@db ~]$ sudo systemctl list-timers
NEXT                        LEFT          LAST                        PASSED       UNIT                         ACTIVATES
Fri 2022-11-18 16:53:22 CET 1h 40min left Fri 2022-11-18 15:12:13 CET 41s ago      dnf-makecache.timer          dnf-makecache.service
Sat 2022-11-19 00:00:00 CET 8h left       Fri 2022-11-18 00:05:38 CET 15h ago      logrotate.timer              logrotate.service
Sat 2022-11-19 04:00:00 CET 12h left      n/a                         n/a          db-dump.timer                db-dump.service
Sat 2022-11-19 15:10:10 CET 23h left      Fri 2022-11-18 15:10:10 CET 2min 45s ago systemd-tmpfiles-clean.timer systemd-tmpfiles-clean.service

4 timers listed.
Pass --all to see loaded but inactive timers, too.
```

➜ **Tester la restauration des données**

```
[ml@db ~]$ sudo unzip /srv/db_dumps/db_nextcloud_221123142423.zip
[ml@db ~]$ mysql -u admin -p nextcloud < /srv/db_dumps/db_nextcloud_221123142423.sql
```
