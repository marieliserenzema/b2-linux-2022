# TP3 : Amélioration de la solution NextCloud

# Module 2 : Réplication de base de données

➜ **Step 1 - Install MariaDB on All Nodes**

🖥️ **VM `db-replication.tp3.linux`**

```
[ml@db-replication ~]$ sudo dnf -y install mariadb-server
```
- Démarrage du service 

```
[ml@db-replication ~]$ sudo systemctl start mariadb
[ml@db-replication ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.
[ml@db-replication ~]$ sudo systemctl is-enabled mariadb
enabled
[ml@db-replication ~]$ sudo systemctl status mariadb
● mariadb.service - MariaDB 10.5 database server
     Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor p>
     Active: active (running) since Mon 2022-11-21 17:22:54 CET; 19s ago
       Docs: man:mariadbd(8)
             https://mariadb.com/kb/en/library/systemd/
   Main PID: 3080 (mariadbd)
     Status: "Taking your SQL requests now..."
      Tasks: 12 (limit: 5907)
     Memory: 75.0M
        CPU: 334ms
     CGroup: /system.slice/mariadb.service
             └─3080 /usr/libexec/mariadbd --basedir=/usr
```

➜ **Step 2 - Prepare the Master Node**

```
[ml@db ~]$ sudo vi /etc/my.cnf
!includedir /etc/my.cnf.d

[mysqld]
bind-address=10.102.1.12
server-id=1
log_bin=mysql-bin
binlog-format=ROW
```
```
[ml@db ~]$ sudo systemctl restart mariadb
```

➜ **Step 3 - Create a Replication User on Master Node**

```
[ml@db ~]$ sudo mysql -u root -p
```

```sql
MariaDB [(none)]> CREATE USER 'replication'@'%' identified by 'pewpewpew';
Query OK, 0 rows affected (0.004 sec)

MariaDB [(none)]> GRANT REPLICATION SLAVE ON *.* TO 'replication'@'%';
Query OK, 0 rows affected (0.002 sec)

MariaDB [(none)]>  FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.001 sec)

MariaDB [(none)]> SHOW MASTER STATUS;
+------------------+----------+--------------+------------------+
| File             | Position | Binlog_Do_DB | Binlog_Ignore_DB |
+------------------+----------+--------------+------------------+
| mysql-bin.000001 |      790 |              |                  |
+------------------+----------+--------------+------------------+
1 row in set (0.000 sec)

MariaDB [(none)]> exit;
Bye
```

```
[ml@db ~]$ sudo mysqldump -u root -p nextcloud >> nextcloud.sql
[ml@db ~]$ scp nextcloud.sql ml@10.102.1.14:~/
ml@10.102.1.14's password:
nextcloud.sql                                                                                    100%  149KB  35.9MB/s   00:00
```

```
[ml@db-replication ~]$ ls
nextcloud.sql
```

➜ **Step 4 - Prepare the Slave Node for Replication**

```
[ml@db-replication ~]$ sudo vi /etc/my.cnf
!includedir /etc/my.cnf.d

[mysqld]
bind-address=10.102.1.14
server-id=2
binlog-format=ROW
```

```
[ml@db-replication ~]$ sudo systemctl restart mariadb
```

```
[ml@db-replication ~]$ sudo mysql -u root -p
```

```sql
MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.000 sec)

MariaDB [(none)]> exit
Bye
```

```
[ml@db-replication ~]$ sudo mysql -u root -p nextcloud<nextcloud.sql
```

```sql
MariaDB [(none)]> STOP SLAVE;
Query OK, 0 rows affected, 1 warning (0.000 sec)

MariaDB [(none)]> CHANGE MASTER TO MASTER_HOST = '10.102.1.12', MASTER_USER = 'replication', MASTER_PASSWORD = 'pewpewpew', MASTER_LOG_FILE = 'mysql-bin.000001', MASTER_LOG_POS = 790;
Query OK, 0 rows affected (0.015 sec)

MariaDB [(none)]> START SLAVE;
Query OK, 0 rows affected (0.001 sec)
```

➜ **Step 5 - Verify MariaDB Replication**

```
[ml@db ~]$ sudo mysql -u root -p
```

```sql
MariaDB [(none)]>  CREATE DATABASE schooldb;
Query OK, 1 row affected (0.000 sec)

MariaDB [(none)]> USE schooldb;
Database changed
MariaDB [schooldb]> CREATE TABLE students (id int, name varchar(20), surname varchar(20));
Query OK, 0 rows affected (0.010 sec)

MariaDB [schooldb]> INSERT INTO students VALUES (1,"hitesh","jethva");
Query OK, 1 row affected (0.002 sec)

MariaDB [schooldb]>  INSERT INTO students VALUES (2,"jayesh","jethva");
Query OK, 1 row affected (0.002 sec)

MariaDB [schooldb]> SELECT * FROM students;
+------+--------+---------+
| id   | name   | surname |
+------+--------+---------+
|    1 | hitesh | jethva  |
|    2 | jayesh | jethva  |
+------+--------+---------+
2 rows in set (0.000 sec)
```

```
[ml@db-replication ~]$ sudo mysql -u root -p
```

```sql
MariaDB [(none)]> SHOW SLAVE STATUS \G
*************************** 1. row ***************************
                Slave_IO_State: Waiting for master to send event
                   Master_Host: 10.102.1.12
                   Master_User: replication
                   Master_Port: 3306
                 Connect_Retry: 60
               Master_Log_File: mysql-bin.000001
           Read_Master_Log_Pos: 1628
                Relay_Log_File: mariadb-relay-bin.000002
                 Relay_Log_Pos: 1249
         Relay_Master_Log_File: mysql-bin.000001
              Slave_IO_Running: Yes
             Slave_SQL_Running: Yes
               Replicate_Do_DB:
           Replicate_Ignore_DB:
            Replicate_Do_Table:
        Replicate_Ignore_Table:
       Replicate_Wild_Do_Table:
   Replicate_Wild_Ignore_Table:
                    Last_Errno: 0
                    Last_Error:
                  Skip_Counter: 0
           Exec_Master_Log_Pos: 1628
               Relay_Log_Space: 1560
               Until_Condition: None
                Until_Log_File:
                 Until_Log_Pos: 0
            Master_SSL_Allowed: No
            Master_SSL_CA_File:
            Master_SSL_CA_Path:
               Master_SSL_Cert:
             Master_SSL_Cipher:
                Master_SSL_Key:
         Seconds_Behind_Master: 0
 Master_SSL_Verify_Server_Cert: No
                 Last_IO_Errno: 0
                 Last_IO_Error:
                Last_SQL_Errno: 0
                Last_SQL_Error:
   Replicate_Ignore_Server_Ids:
              Master_Server_Id: 1
                Master_SSL_Crl:
            Master_SSL_Crlpath:
                    Using_Gtid: No
                   Gtid_IO_Pos:
       Replicate_Do_Domain_Ids:
   Replicate_Ignore_Domain_Ids:
                 Parallel_Mode: optimistic
                     SQL_Delay: 0
           SQL_Remaining_Delay: NULL
       Slave_SQL_Running_State: Slave has read all relay log; waiting for more updates
              Slave_DDL_Groups: 2
Slave_Non_Transactional_Groups: 0
    Slave_Transactional_Groups: 2
1 row in set (0.000 sec)

MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| nextcloud          |
| performance_schema |
| schooldb           |
+--------------------+
5 rows in set (0.001 sec)

MariaDB [(none)]> USE schooldb;
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
MariaDB [schooldb]> SHOW TABLES;
+--------------------+
| Tables_in_schooldb |
+--------------------+
| students           |
+--------------------+
1 row in set (0.000 sec)

MariaDB [schooldb]> SELECT * FROM students;
+------+--------+---------+
| id   | name   | surname |
+------+--------+---------+
|    1 | hitesh | jethva  |
|    2 | jayesh | jethva  |
+------+--------+---------+
2 rows in set (0.001 sec)
```
